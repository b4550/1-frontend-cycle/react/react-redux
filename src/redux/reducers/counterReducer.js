const initialState = {
    count: 0
}

const counterReducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD":
            return {
                count: state.count + action.payload
            }
        case "DEL":
            return {
                count: state.count - action.payload
            }
        case "RES":
            return {
                count: initialState.count
            }
        default:
            return state;
    }
}

export default counterReducer