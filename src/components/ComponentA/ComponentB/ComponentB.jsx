import React from "react";
import { useDispatch } from "react-redux";
import { addNum, delNum, resNum } from "../../../redux/actions/counterAction";

const ComponentB = () => {
  // useDispatch
  const dispatch = useDispatch();
  // ! IGNORE THE CODE BELOW
  const addCode = "None";
  const delCode = "None";
  const resCode = "None";
  // ! IGNORE THE CODE ABOVE
  return (
    <div className="ComponentB">
      <h1>ComponentB</h1>
      {/* // ! DON'T PAY ATTENTION */}
      <pre>
        State: <code>None</code>
      </pre>
      <pre>
        Child: <code>None</code>
      </pre>
      {/* // ! DON'T PAY ATTENTION */}

      <button className="btn" onClick={() => dispatch(addNum())}>
        ADD
      </button>
      <button className="btn" onClick={() => dispatch(delNum())}>
        DEL
      </button>
      <button className="btn" onClick={() => dispatch(resNum())}>
        RESET
      </button>

      <hr />
      {/* // ! DON'T PAY ATTENTION */}
      <pre>
        ADD: <code>{addCode}</code>
      </pre>
      <pre>
        DEL: <code>{delCode}</code>
      </pre>
      <pre>
        RESET: <code>{resCode}</code>
      </pre>
      {/* // ! DON'T PAY ATTENTION */}
      <hr />
    </div>
  );
};

export default ComponentB;
