import React from "react";
import { useSelector } from "react-redux";

const GlobalState = () => {
  // useState
  // useEffect
  // useSelector
  // useDispatch
  const count = useSelector((state) => state.counter.count);
  const movieId = useSelector((state) => state.movie.movieId);
  console.log("state", count);

  const countCode = `counter: { count: ${count} }`;
  const movieCode = `movie: { movieId: ${movieId} }`;
  return (
    <div className="GlobalState">
      <pre style={{ color: "red", fontSize: "24px" }}>GlobalState</pre>
      <pre>
        State:
        <br />
        <code>{countCode}</code>
        <br />
        <code>{movieCode}</code>
      </pre>
    </div>
  );
};

export default GlobalState;
